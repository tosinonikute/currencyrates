Currency Rates

## What I did?

I created a single screen app that fetches currency data every 60 seconds & shows user a message via the Snack bar.

## My choice of Architecture - MVVM

It enforces Separation of concerns & has the benefits of easier testing and modularity. As activity is never responsible for the requests for fetching data, the ViewModels can handle that efficiently.
ViewModels are classes that interacts with the logic/model layer and just exposes states/data and actually has no idea by whom or how that data will be consumed. Only View(Activity) holds the reference to ViewModel and not vice versa, this solves our tight coupling issue. A single view can hold reference to multiple ViewModels.

## This Application is developed entirely in Kotlin

## Libraries used

* Glide - for image loading
* Anko library - for fast and type-safe dynamic Android layouts
* Retrofit2 - type-safe REST client to make API request.
* Kotlin Coroutines - for async callbacks, managing background threads & long-running tasks such as network requests
* MOSHI - as my Retrofit adapter & for parsing the JSON responses
* Okhttp - used effectively for logging network errors
* JUnit - for Unit testing
* Espresso - for User Interface testing





