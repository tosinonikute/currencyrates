package com.s.currencyrates.utils.widget

import android.app.Dialog
import android.content.Context
import android.view.Gravity
import android.view.WindowManager
import com.s.currencyrates.R

class Progressbar private constructor(context: Context, theme: Int) : Dialog(context, theme) {
    companion object {
        fun show(context: Context): Progressbar {
            val dialog = Progressbar(context, R.style.Theme_ProgressDialog)
            dialog.setTitle("")
            dialog.setContentView(R.layout.layout_progress_bar)
            dialog.setCancelable(true)
            dialog.window!!.attributes.gravity = Gravity.CENTER
            val lp = dialog.window!!.attributes
            lp.dimAmount = 0.2f
            dialog.window!!.attributes = lp
            dialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            dialog.show()
            return dialog
        }
    }
}