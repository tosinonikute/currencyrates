package com.s.currencyrates.utils.moshi

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi

inline fun <reified T : Any> Moshi.adapter(): JsonAdapter<T> {
    return this.adapter(T::class.java)
}

fun <T> JsonAdapter<T>.fromJsonString(string: String): T {
    return this.fromJson(string)!!
}