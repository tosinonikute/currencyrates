package com.s.currencyrates.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.request.RequestOptions
import com.s.currencyrates.R
import org.jetbrains.anko.runOnUiThread


object Utils {

    private const val TAG = "Utils"

    fun showDialog(context: Context, title: String, message: String) {
        context.runOnUiThread {
            try {
                val alertDialog = AlertDialog.Builder(context, R.style.DialogTheme).create()
                alertDialog.setTitle(title)
                alertDialog.setMessage(message)
                alertDialog.setButton(
                    AlertDialog.BUTTON_POSITIVE,
                    getButtonTextSpan(context, context.getString(R.string.close))
                ) { dialog, _ -> dialog.dismiss() }
                alertDialog.show()
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE)
                    .setTextColor(ContextCompat.getColor(context, R.color.dark))

            } catch (e: Exception) {
                Log.e(TAG, "Exception --- > " + e.message)
            }
        }
    }

    private fun getButtonTextSpan(context: Context, text: String): SpannableStringBuilder {
        return SpannableStringBuilder(text)
    }

    fun setImage(context: Context, drawable: Drawable, image: AppCompatImageView){
        GlideApp.with(context)
            .load(drawable)
            .apply(RequestOptions.circleCropTransform())
            .into(image)
        image.setPadding(0, 0, 0, 0)
    }
}