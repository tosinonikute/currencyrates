package com.s.currencyrates.model

import com.squareup.moshi.Json

data class Rates(

	@field:Json(name="HRK")
	val hRK: Double? = null,

	@field:Json(name="CHF")
	val cHF: Double? = null,

	@field:Json(name="MXN")
	val mXN: Double? = null,

	@field:Json(name="ZAR")
	val zAR: Double? = null,

	@field:Json(name="INR")
	val iNR: Double? = null,

	@field:Json(name="THB")
	val tHB: Double? = null,

	@field:Json(name="CNY")
	val cNY: Double? = null,

	@field:Json(name="AUD")
	val aUD: Double? = null,

	@field:Json(name="ILS")
	val iLS: Double? = null,

	@field:Json(name="KRW")
	val kRW: Double? = null,

	@field:Json(name="JPY")
	val jPY: Double? = null,

	@field:Json(name="PLN")
	val pLN: Double? = null,

	@field:Json(name="GBP")
	val gBP: Double? = null,

	@field:Json(name="IDR")
	val iDR: Double? = null,

	@field:Json(name="HUF")
	val hUF: Double? = null,

	@field:Json(name="PHP")
	val pHP: Double? = null,

	@field:Json(name="TRY")
	val tRY: Double? = null,

	@field:Json(name="RUB")
	val rUB: Double? = null,

	@field:Json(name="HKD")
	val hKD: Double? = null,

	@field:Json(name="ISK")
	val iSK: Double? = null,

	@field:Json(name="DKK")
	val dKK: Double? = null,

	@field:Json(name="CAD")
	val cAD: Double? = null,

	@field:Json(name="USD")
	val uSD: Double? = null,

	@field:Json(name="MYR")
	val mYR: Double? = null,

	@field:Json(name="BGN")
	val bGN: Double? = null,

	@field:Json(name="NOK")
	val nOK: Double? = null,

	@field:Json(name="RON")
	val rON: Double? = null,

	@field:Json(name="SGD")
	val sGD: Double? = null,

	@field:Json(name="CZK")
	val cZK: Double? = null,

	@field:Json(name="SEK")
	val sEK: Double? = null,

	@field:Json(name="NZD")
	val nZD: Double? = null,

	@field:Json(name="BRL")
	val bRL: Double? = null
)