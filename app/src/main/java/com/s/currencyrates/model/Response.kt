package com.s.currencyrates.model

import com.squareup.moshi.Json

class ApiResponse<T>(

	@field:Json(name = "date")
	val date: String,

	@field:Json(name = "rates")
	val rates: T,

	@field:Json(name = "base")
	val base: String
) {
	override fun toString(): String {
		return "ApiResponse(date='$date', rates=$rates, base='$base')"
	}
}