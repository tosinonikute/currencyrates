package com.s.currencyrates.model

data class Rate (
    val name: String,
    val amount: Double
)