package com.s.currencyrates

import android.app.Application
import com.s.currencyrates.utils.extensions.NotNullSingleValueVar

class BaseApplication : Application() {
    companion object {
        var INSTANCE by NotNullSingleValueVar<BaseApplication>()
    }

    override fun onCreate() {
        super.onCreate()
        INSTANCE = this
    }
}