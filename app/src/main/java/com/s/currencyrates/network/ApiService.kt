package com.s.currencyrates.network

import com.google.gson.JsonObject
import com.s.currencyrates.model.ApiResponse
import com.s.currencyrates.model.Rates
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*


interface ApiService {

    @GET("latest")
    fun fetchRates(): Deferred<Response<ApiResponse<Rates>>>

}