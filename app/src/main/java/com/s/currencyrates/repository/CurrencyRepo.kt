package com.s.currencyrates.repository

import androidx.lifecycle.MutableLiveData
import com.s.currencyrates.model.ApiResponse
import kotlinx.coroutines.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import retrofit2.Response


fun <RESPONSE : Any> fetchData(liveData: MutableLiveData<Resource<RESPONSE>>, block: FeedsCallHandler<RESPONSE>.() -> Unit) {
    return FeedsCallHandler<RESPONSE>().apply(block).makeCall(liveData)
}

class FeedsCallHandler<RESPONSE : Any> : AnkoLogger {
    lateinit var client: Deferred<Response<ApiResponse<RESPONSE>>>

    fun makeCall(result: MutableLiveData<Resource<RESPONSE>>) {
        result.value = Resource.Loading(true)
        GlobalScope.launch(Dispatchers.IO) {
            try {
                val response = client.await()
                if (response.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        result.value = Resource.Success(response.apiResponseData())
                        result.value = Resource.Base(response.body()!!.base)
                        result.value = Resource.Date(response.body()!!.date)
                    }
                } else {
                    withContext(Dispatchers.Main) {
                        result.value = Resource.Error(response.code(), response.errorBody()!!)
                    }
                }

            } catch (ex: Exception) {
                error("Error occurred when making network call", ex)
                withContext(Dispatchers.Main) {
                    result.value = Resource.Loading(false)
                    result.value = Resource.Exception()
                }
            }
        }
    }
}

fun <T> Response<ApiResponse<T>>.apiResponseData(): T {
    return this.body()!!.rates
}
