package com.s.currencyrates.repository

import okhttp3.ResponseBody

sealed class Resource<T> {
    class Success<T>(val rates: T) : Resource<T>() {
        override fun toString(): String {
            return "Resource.Success{rates=$rates}"
        }
    }
    class Error<T>(val httpStatusCode: Int, val errorBody: ResponseBody) : Resource<T>()
    class Loading<T>(val isLoading: Boolean) : Resource<T>()
    class Date<T>(val date: String) : Resource<T>()
    class Base<T>(val base: String) : Resource<T>()
    class Exception<T> : Resource<T>()
}