package com.s.currencyrates.ui.list

import androidx.lifecycle.MutableLiveData
import com.s.currencyrates.model.Rates
import com.s.currencyrates.network.ApiClient
import com.s.currencyrates.repository.Resource
import com.s.currencyrates.repository.fetchData
import com.s.currencyrates.ui.base.BaseViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.ticker
import kotlin.coroutines.CoroutineContext

class MainActivityViewModel : BaseViewModel(), CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + SupervisorJob()

    val response = MutableLiveData<Resource<Rates>>()

    // Using TickerChannel to produce values every X millis
    // A comparable usage of Flowable.interval(0, 5, TimeUnit.SECONDS) for RxJava
    @kotlinx.coroutines.ObsoleteCoroutinesApi
    private val tickerChannel = ticker(delayMillis = 60_000, initialDelayMillis = 0)

    private fun getAllRates() {
        fetchData(response) {
            client = ApiClient.apiService.fetchRates()
        }
    }

    @kotlinx.coroutines.ObsoleteCoroutinesApi
    fun getAllRatesAfterXMinutes(){
        GlobalScope.launch {
            for (event in tickerChannel) {
                withContext(Dispatchers.Main) {
                    getAllRates()
                }
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        coroutineContext.cancel()
    }
}