package com.s.currencyrates.ui.base

import androidx.lifecycle.ViewModel

abstract class BaseViewModel : ViewModel()