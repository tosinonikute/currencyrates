package com.s.currencyrates.ui.list.adapter

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.s.currencyrates.R

class RatesViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val name by lazy { view.findViewById(R.id.name) as AppCompatTextView }
    val amount by lazy { view.findViewById(R.id.amount) as AppCompatTextView }
    val image by lazy { view.findViewById(R.id.image) as AppCompatImageView }
    val currencyName by lazy { view.findViewById(R.id.currency_name) as AppCompatTextView }
}