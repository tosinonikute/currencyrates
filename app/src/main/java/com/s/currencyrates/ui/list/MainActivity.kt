package com.s.currencyrates.ui.list

import android.os.Bundle
import com.s.currencyrates.R
import com.s.currencyrates.model.Rate
import com.s.currencyrates.model.Rates
import com.s.currencyrates.repository.Resource
import com.s.currencyrates.ui.base.BaseActivity
import com.s.currencyrates.ui.list.adapter.RatesAdapter
import com.s.currencyrates.utils.extensions.observe
import com.s.currencyrates.utils.extensions.withViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {

    private lateinit var viewModel: MainActivityViewModel
    private lateinit var adapter: RatesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         viewModel = withViewModel{}
         getRates()
    }

    private fun getRates(){
        if(!::adapter.isInitialized) {
            handleLoading(true)
        }
        viewModel = withViewModel<MainActivityViewModel> {
            if (!response.hasActiveObservers()) {
                observe(response) {
                    handleSuccessResult(it)
                }
            }
        }
        viewModel.getAllRatesAfterXMinutes()
    }

    private fun handleSuccessResult(Resource: Resource<Rates>?) {
        when (Resource) {
            is Resource.Error -> handleResponseError(Resource.httpStatusCode, Resource.errorBody.string())
            is Resource.Exception -> handleResponseException()
            is Resource.Success -> {
                handleLoading(false)

                val rates = Resource.rates
                val list = mutableListOf<Rate>()

                if(::adapter.isInitialized){
                    displayRefreshMessage(getString(R.string.currency_refreshing))
                } else {
                    displayRefreshMessage(getString(R.string.currency_show))
                }

                list.add(Rate(applicationContext.resources.getString(R.string.usd), rates.uSD!!))
                list.add(Rate(applicationContext.resources.getString(R.string.pnl), rates.pLN!!))
                adapter = RatesAdapter(applicationContext, list)
                recyclerRates.adapter = adapter

            }
        }
    }

    private fun displayRefreshMessage(str: String){
        displaySnackBar(str, this, false)
        android.os.Handler().postDelayed({
            hideSnackBar()
        }, 4000)
    }
}
