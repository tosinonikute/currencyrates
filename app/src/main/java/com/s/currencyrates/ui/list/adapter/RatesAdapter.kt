package com.s.currencyrates.ui.list.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.s.currencyrates.R
import com.s.currencyrates.model.Rate
import com.s.currencyrates.utils.Utils

class RatesAdapter(val context: Context, val mItem: MutableList<Rate>?): RecyclerView.Adapter<RatesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatesViewHolder {
        return RatesViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.list_rates, parent, false))
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(@NonNull holder: RatesViewHolder, position: Int) {
        val model = mItem!![holder.adapterPosition]
        holder.name.text = model.name
        holder.amount.text = model.amount.toString()

        when(model.name){
            context.resources.getString(R.string.pnl) -> {
                Utils.setImage(context, ContextCompat.getDrawable(context, R.drawable.flag_pl)!!, holder.image)
                holder.currencyName.text = context.resources.getString(R.string.pnl_string)
            }
            context.resources.getString(R.string.usd) -> {
                Utils.setImage(context, ContextCompat.getDrawable(context, R.drawable.flag_us)!!, holder.image)
                holder.currencyName.text = context.resources.getString(R.string.usd_string)
            }
        }
    }

    override fun getItemCount(): Int{
        return mItem!!.size
    }
}