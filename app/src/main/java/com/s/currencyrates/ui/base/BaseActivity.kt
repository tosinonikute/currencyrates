package com.s.currencyrates.ui.base

import android.app.Activity
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.s.currencyrates.R
import com.s.currencyrates.utils.Utils
import com.s.currencyrates.utils.widget.Progressbar
import org.json.JSONObject

abstract class BaseActivity : AppCompatActivity() {

    private lateinit var progressbar: Progressbar
    private lateinit var snackBarMsg: Snackbar

    companion object {
        private const val TAG = "BaseActivity"
    }

    private fun showProgressBar() {
        if (!isFinishing) {
            progressbar = Progressbar.show(this)
        }
    }

    private fun hideProgressBar() {
        try {
            if (::progressbar.isInitialized) {
                progressbar.dismiss()
            }
        } catch (e: Exception) {
            Log.e(TAG, Log.getStackTraceString(e))
        }
    }

    fun handleLoading(loading: Boolean){
        if(loading){
            showProgressBar()
        } else {
            hideProgressBar()
        }
    }

    fun handleResponseError(code: Int, errorBody: String) {
        handleLoading(false)
        val activity = this
        when (code) {
            400, 401, 422, 403 -> {
                Utils.showDialog(activity, getString(R.string.error_title) , getErrorMessage(errorBody))
            }
            504 -> Utils.showDialog(activity, getString(R.string.error_title) , getString(R.string.connection_timeout_title))
            else -> Utils.showDialog(activity, getString(R.string.error_title) , getErrorMessage(errorBody))
        }
    }

    private fun getErrorMessage(errorBody: String): String {
        try {
            val errorObject = JSONObject(errorBody)
            return errorObject.getString("error")
        } catch (e: Exception) {
            Log.d(TAG, e.localizedMessage)
        }
        return getString(R.string.something_went_wrong)
    }

    fun handleResponseException() {
        handleLoading(false)
        displaySnackBar(getString(R.string.connection_no_network), this, true)
    }

    fun displaySnackBar(str: String, activity: Activity, showCloseButton: Boolean){
        snackBarMsg = Snackbar.make(findViewById(android.R.id.content), str, Snackbar.LENGTH_INDEFINITE)
        if(showCloseButton){
            snackBarMsg.setAction("Close", View.OnClickListener {
                snackBarMsg.dismiss() })
        }
        snackBarMsg.setActionTextColor(ContextCompat.getColor(activity, R.color.colorWhite))
        val sbView = snackBarMsg.view
        sbView.setBackgroundColor(ContextCompat.getColor(activity, R.color.colorPrimaryDark))
        snackBarMsg.show()
    }

    fun hideSnackBar(){
        if(::snackBarMsg.isInitialized){
            snackBarMsg.dismiss()
        }
    }
}