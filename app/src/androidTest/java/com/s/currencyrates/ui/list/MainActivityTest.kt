package com.s.currencyrates.ui.list


import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import androidx.test.runner.AndroidJUnit4
import com.s.currencyrates.R
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers.*
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.core.IsInstanceOf
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    var mActivityTestRule = ActivityTestRule(MainActivity::class.java)

    var recyclerView: RecyclerView? = null

    private val textOne = "Showing Rates!"
    private val textTwo = "Refreshing Rates..."

    @Test
    fun mainActivityTest() {

        // Wait for 2 Seconds
        Thread.sleep(2000)

        val textView = onView(
            allOf(
                withId(R.id.snackbar_text), withText(textOne),
                childAtPosition(
                    childAtPosition(
                        IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView.check(matches(withText(textOne)))


        recyclerView = mActivityTestRule.activity.findViewById(R.id.recyclerRates)
        val viewHolder1 = recyclerView!!.findViewHolderForAdapterPosition(0)
        val viewHolder2 = recyclerView!!.findViewHolderForAdapterPosition(1)
        val currencyTitle1 = (viewHolder1!!.itemView.findViewById<View>(R.id.amount) as AppCompatTextView).text.toString()
        val currencyTitle2 = (viewHolder2!!.itemView.findViewById<View>(R.id.amount) as AppCompatTextView).text.toString()


        // Assert that api returns Non empty amount as Strings
        assertThat(currencyTitle1, not(isEmptyString()))
        assertThat(currencyTitle2, not(isEmptyString()))


        // Wait for 1 Minute
        Thread.sleep(60000)


        // Verify that there was actually a Refresh message shown to the user
        val textView4 = onView(
            allOf(
                withId(R.id.snackbar_text), withText(textTwo),
                childAtPosition(
                    childAtPosition(
                        IsInstanceOf.instanceOf(android.widget.FrameLayout::class.java),
                        0
                    ),
                    0
                ),
                isDisplayed()
            )
        )
        textView4.check(matches(withText(textTwo)))

    }

    private fun childAtPosition(
        parentMatcher: Matcher<View>, position: Int
    ): Matcher<View> {

        return object : TypeSafeMatcher<View>() {
            override fun describeTo(description: Description) {
                description.appendText("Child at position $position in parent ")
                parentMatcher.describeTo(description)
            }

            public override fun matchesSafely(view: View): Boolean {
                val parent = view.parent
                return parent is ViewGroup && parentMatcher.matches(parent)
                        && view == parent.getChildAt(position)
            }
        }
    }
}
